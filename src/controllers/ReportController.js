const { Op } = require('sequelize');
const Tech = require('../models/Tech');
const User = require('../models/User');

module.exports = {
    async show(req, res) {
        // Encontrar os usuários que possuem email com final @rocketseat.com.br
        // Desses usuários eu quero buscar todos que moram na rua "8th Street"
        // Desses usuários eu quero buscar as tecnologias que começam com React

        const users = await User.findAll({
            attributes: ['name', 'email'],
            where: {
                email: { 
                    [Op.iLike]: '%@rocketseat.com.br'
                }
            },
            include: [
                { association: 'addresses', where: { street: 
                    '8th Street'
                    } 
                },
                { 
                    association: 'techs',
                    required: false,
                    where: {
                        name:{
                            [Op.iLike]: 'React%'
                        }
                    } 
                }, 
            ]
        })

        return res.json(users);
    }
}