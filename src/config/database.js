module.exports = {
    dialect: 'postgres',
    host: 'localhost',
    username: 'postgres',
    password: 'iamcatholic',
    database: 'sqlnode',
    define: {
        timestamps: true,
        underscored: true,
    },
};