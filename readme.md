# SQL no Node.JS usando PostgreSQL e Sequelize
## Projeto desenvolvido seguindo o tutorial disponível no youtube: [Masterclass #01 - SQL no Node.js com Sequelize](https://www.youtube.com/watch?v=Fbu7z5dXcRs)


# Comandos úteis
> yarn install

> yarn sequelize db:migrate
### ou para desfazer
>yarn sequelize db:migrate:undo

### Para criar uma nova migration
> yarn sequelize migration:create --name=create-projects

> yarn sequelize migration:create --name=create-user_projects